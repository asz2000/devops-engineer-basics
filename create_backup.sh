#!/bin/bash

rm -f /var/www/html/files/*.tar.gz
aname=`date '+%d%m%y'`
pushd /var/www/html
tar -zcf /var/www/html/files/backup$aname.tar.gz *
popd

echo '<html><head><title>Archive APP</title></head>' >/var/www/html/archive.html
echo "<body><h1>Archive app</h1> <a href=\"files/backup$aname.tar.gz\" download=\"\">Archive $aname</a>" >>/var/www/html/archive.html
echo '</body></html>' >>/var/www/html/archive.html
